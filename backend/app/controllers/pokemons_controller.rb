Data = Struct.new(:code, :data)
class PokemonsController < ApplicationController
  before_action :set_pokemon, only: [:show, :update, :destroy]

  def index
    begin 
      @pokemons = cached_result("https://pokeapi.co/api/v2/pokemon?offset=#{params[:offset]}&limit=#{params[:limit]}")
      if @pokemons.code == 403
        render json: {errCode: 'ERR1001', errMsg: 'Unauthorized to Third Party Provider', data: [] }
      else
        render json: {errCode: '', errMsg: '', data: @pokemons.data }
      end    
    rescue => e
      @pokemons = []
      render json: {errCode: 'ERR1000', errMsg: "An error of type #{e.class} happened, message is #{e.message}", data: @pokemons }
    end  
  end

  # GET /pokemons/1
  def show
    begin 
      @pokemon = HTTParty.get("https://pokeapi.co/api/v2/pokemon/#{params[:id]}")
      if @pokemon.code == 403
        render json: {errCode: 'ERR1001', errMsg: 'Unauthorized to TTPPPPP', data: [] }
      else
        render json: {errCode: '', errMsg: '', data: @pokemon }
      end    
    rescue => e
      render json: {errCode: 'ERR1000', errMsg: "An error of type #{e.class} happened, message is #{e.message}", data: {} }
    end  
  end

  def cached_result(key)
    Rails.cache.fetch(key.to_sym, :expires => 10.minutes) do
      reponse_pokemons = HTTParty.get(key)
      pokemons = { 
        count: reponse_pokemons.parsed_response["count"],
        next: reponse_pokemons.parsed_response["next"], 
        previous: reponse_pokemons.parsed_response["previous"],
        results: []
      }

      reponse_pokemons.parsed_response["results"].each do |basic_data_pokemon|
        pokemon_id = basic_data_pokemon["url"].split('/').last.to_i
        response_pokemon = HTTParty.get("https://pokeapi.co/api/v2/pokemon/#{pokemon_id}/")
        pokemon = response_pokemon.parsed_response
        pokemons[:results] << pokemon
      end
      Data.new reponse_pokemons.code, pokemons
    end
  end 

  # POST /pokemons
  def create
    @pokemon = Pokemon.new(pokemon_params)

    if @pokemon.save
      render json: @pokemon, status: :created, location: @pokemon
    else
      render json: @pokemon.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /pokemons/1
  def update
    if @pokemon.update(pokemon_params)
      render json: @pokemon
    else
      render json: @pokemon.errors, status: :unprocessable_entity
    end
  end

  # DELETE /pokemons/1
  def destroy
    @pokemon.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pokemon
      #@pokemon = Pokemon.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pokemon_params
      params.fetch(:pokemon, {})
    end
end
