import React, { Component } from "react";
import PokemonDataService from "../services/pokemon.service";

export default class Pokemon extends Component {
  constructor(props) {
    super(props);
    this.getPokemon = this.getPokemon.bind(this);

    this.state = {
      currentPokemon: {
        id: null,
        name: "",
        url: "",
        sprites: null
      }
    };
  }

  componentDidMount() {
    this.getPokemon(this.props.match.params.id);
  }

  getPokemon(id) {
    PokemonDataService.get(id)
      .then(response => {
        this.setState({
          currentPokemon: response.data.data
        });
        console.log('este es el reponse');
        console.log(response)
      })
      .catch(e => {
        console.log('se vino por aca')
        console.log(e);
      });
  }

  render() {
    const { currentPokemon } = this.state;
    return (
      <div>
                 
        {currentPokemon.sprites ? (
            <div className="page-content page-container" id="page-content">
            <div className="padding">
                <div className="row container d-flex justify-content-center">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-body text-center">
                                <div> 
                                    <img src={currentPokemon.sprites.front_default} className="img-lg rounded-circle mb-4" alt="profile image"/>
                                    <img src={currentPokemon.sprites.back_default} className="img-lg rounded-circle mb-4" alt="profile image"/>

                                    <h4>{currentPokemon.name}</h4>
                                    <p className="text-muted mb-0"></p>
                                </div>
                                <div className="row">
                                  <div className="col-md-4">
                                    <p className="mt-2 card-text"> 
                                      <b>Abilities</b><br></br>
                                      {currentPokemon.abilities.map((a, index) => (
                                        (currentPokemon.abilities.length - 1) == index ? (a.ability.name) : (a.ability.name + ", ")
                                      ))}
                                    </p>
                                  </div>
                                  <div className="col-md-4">
                                    <p className="mt-2 card-text"> 
                                        <b>Types</b><br></br>
                                        {currentPokemon.types.map((a, index) => (
                                          (currentPokemon.types.length - 1) == index ? (a.type.name) : (a.type.name + ", ")
                                        ))}
                                      </p>
                                  </div>
                                  <div className="col-md-4">
                                    <p className="mt-2 card-text"> 
                                        <b>Weight</b><br></br>
                                        {currentPokemon.weight}
                                      </p>
                                  </div>
                                </div>
                                
                                <br></br>
                                <div className="border-top pt-3">
                                    <h4>Evolutions</h4>
                                    <div className="row">
                                        <div className="col-4">
                                            <h6>evolution 1</h6>
                                            <p>1</p>
                                        </div>
                                        <div className="col-4">
                                            <h6>evolution 2</h6>
                                            <p>2</p>
                                        </div>
                                        <div className="col-4">
                                            <h6>evolution 3</h6>
                                            <p>3</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        ) : (
          <div>
            <br />
            <p>Loading...</p>
          </div>
        )}
      </div>
    );
  }
}
