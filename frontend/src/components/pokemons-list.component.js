import React, { Component } from "react";
import PokemonDataService from "../services/pokemon.service";
import { Link } from "react-router-dom";


export default class PokemonsList extends Component {
  constructor(props) {
    super(props);
    this.retrievePokemons = this.retrievePokemons.bind(this);

    this.setActivePokemon = this.setActivePokemon.bind(this);
    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);



    this.state = {
      pokemons: [],
      currentPokemon: null,
      currentIndex: -1,
      searchTitle: "",
      loading: true,
      offset: 0,
      loadNumber: 5,   
    };
  }

  getNextOffset() {
    return this.state.offset+this.state.loadNumber;
  }

  getPrevOffset() {
    return this.state.offset-this.state.loadNumber;
  }

  next(event) {
    const newOffset = this.getNextOffset();
    this.setState({offset: newOffset}, () => {
      console.log("Offset: " + this.state.offset)
      this.retrievePokemons();
    });
    
  }

  prev(event) {
    const newOffset = this.getPrevOffset();
    this.setState({offset: newOffset}, () => {
      console.log("Offset: " + this.state.offset)
      this.retrievePokemons();
    });
    
  }


  componentDidMount() {
    this.retrievePokemons();
  }

  retrievePokemons() {
    let url = "?offset=" + this.state.offset + "&limit=" + this.state.loadNumber;
    
    PokemonDataService.getAll(url)
      .then(response => {
        this.setState({
          pokemons: response.data.data.results,
          loading: false
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  setActivePokemon(pokemon, index) {
    this.setState({
      currentPokemon: pokemon,
      currentIndex: index
    });
  }


  render() {
    const { pokemons, currentPokemon, loading, currentIndex, offset } = this.state;

    return (
      <div className="row">
        <div className="col-md-12">
          <h4>Pokemons List</h4>
            {(loading == false) ? (
              <div>
              <table className="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Weight</th>
                    <th>Abilities</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {pokemons && pokemons.map((pokemon, index) => (
                      <tr className={
                        (index === currentIndex ? "table-primary" : "")
                        }
                        onClick={() => this.setActivePokemon(pokemon, index)}
                        key={index}>
                        <td><img src={pokemon.sprites.front_default} className="img-lg rounded-circle mb-4" alt="profile image"/></td>
                        <td>{pokemon.name}</td>
                        <td></td>
                        <td>{pokemon.weight}</td>
                        <td>
                            {pokemon.abilities.map((a, index) => (
                            (pokemon.abilities.length - 1) == index ? (a.ability.name) : (a.ability.name + ", ")
                            ))}
                        </td>
                        <td>
                          {currentPokemon && (currentPokemon.id == pokemon.id) ? (
                          <div>
                              <Link
                                    to={"/pokemons/" + currentPokemon.id}
                                    className="badge badge-warning"
                                  >
                                    Details
                                  </Link>
                                </div>
                              ) : (
                                <div>
                                  <p></p>
                                </div>
                              )}
                        </td>
                      </tr>
                    ))}               
                </tbody>
              </table>
                
            
              <div>
                <div className="row">
                  <div className="col-md-6">

                     { offset > 0 ? (
                        <div>
                          <button type="button" className="btn btn-secondary btn-block" key="more-button" id="more-button" onClick={this.prev}>Prev</button>                    

                        </div>
                      ) : (
                        <div>
                        </div>
                      )}

                  </div>  
                   <div className="col-md-6">
                    <button type="button" className="btn btn-secondary btn-block" key="more-button" id="more-button" onClick={this.next}>Next</button>                    

                  </div>  
                    
                </div>
               </div>
               <br></br>                   
              
              </div>


            ) : (
              <div>
                  <p>Loading ...</p>
              </div>
            )}
           
             
         

          
        </div>
       
      </div>
    );
  }
}
