import axios from "axios";

export default axios.create({
  baseURL: 'https://fierce-brook-26681.herokuapp.com',
  headers: {'Access-Control-Allow-Origin': '*'}
});